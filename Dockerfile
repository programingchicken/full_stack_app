From node:latest

RUN npm install

RUN mkdir -p /client

COPY . /client

CMD ["node", "server.js"]
